VAGRANTFILE_API_VERSION = '2'

# Server Configuration
# Set a local private network IP address.
# See http://en.wikipedia.org/wiki/Private_network for explanation
# You can use the following IP ranges:
#   10.0.0.1    - 10.255.255.254
#   172.16.0.1  - 172.31.255.254
#   192.168.0.1 - 192.168.255.254

box                     = 'debian/jessie64'
host_os_document_root   = '~/Dropbox/Dev' # d:/ or ~/
# host_os_document_root = 'd:/Dropbox/Dev'
server_document_root    = '/srv'
hostname                = 'lacodda.dev'
server_ip               = '192.168.80.20'
server_cpus             = '1' # Cores
server_memory           = '512' # MB

# UTC        for Universal Coordinated Time
# EST        for Eastern Standard Time
# CET        for Central European Time
# US/Central for American Central
# US/Eastern for American Eastern
# Europe/Madrid	for Europe Madrid

server_timezone         = 'Europe/Samara'

github_token             = 'e34430d9644ee4ca91648aca5afc405a0b576c23'


####
# Web Servers
##########

# NGINX
nginx_install            = true


####
# Databases
##########

# MySQL
mysql_install            = true
mysql_version            = '5.6' # Options: 5.5 | 5.6 | ONLY MANUAL 5.7
mysql_root_password      = '1111' # We'll assume user 'root'
mysql_enable_remote      = 'true' # remote access enabled when true

# PostgreSQL
postgresql_install       = true
postgresql_version       = '9.5' # Options: 9.5 | 9.4 | 9.3 | 9.2 | etc.
postgresql_root_password = '1111' # We'll assume user 'root'

# RethinkDB
rethinkdb_install        = false

# MongoDB
mongodb_install          = true
mongodb_version          = '3.2' # Options: 2.6 | 3.2 | 3.3
mongodb_enable_remote    = 'false' # remote access enabled when true

# MariaDB
mariadb_install          = false
mariadb_version          = '10.1' # Options: 10.1 | 10.0
mariadb_root_password    = '1111' # We'll assume user 'root'
mariadb_enable_remote    = 'false' # remote access enabled when true

# Neo4J
neo4j_install            = false


####
# In-Memory Stores
##########

# Redis
redis_install            = true
redis_persistent         = 'false'


####
# Languages and Packages
##########

# PHP
php_install              = true
php_version              = ['5.6', 'latest'] # Options: 5.5 | 5.6 | latest
php_timezone             = server_timezone # http://php.net/manual/en/timezones.php
composer_packages        = [# List any global Composer packages that you want to install
		'fxp/composer-asset-plugin',
		'codeception/codeception',
		'phpspec/phpspec',
		'squizlabs/php_codesniffer',
		'laravel/installer',
		'symfony/symfony-installer',
]

# Python
python_install           = true
python_version           = 'latest' # Choose what python version should be installed (will also be the default version)
python_pips              = [# List any Python Pips that you want to install
		'Django',
		'psycopg2',
		'pycurl',
		'lxml',
		# 'Grab',
		'gunicorn',
		'biplist', # Binary plist parser/generator for Python
		'httpie', # HTTPie (pronounced aitch-tee-tee-pie) is a command line HTTP client
		'eve', # Eve is an open source Python REST API framework designed for human beings
		'Click', # Click is a Python package for creating beautiful command line interfaces in a composable way with as little code as necessary.
		'newspaper3k', # News extraction, article extraction and content curation in Python
		'requests', # Requests is an elegant and simple HTTP library for Python, built for human beings
		'mycli', # Terminal Client for MySQL with AutoCompletion and Syntax Highlighting.
		'pgcli', # Postgres CLI with autocompletion and syntax highlighting
		'selenium', # Selenium Python bindings provides a simple API to write functional/acceptance tests using Selenium WebDriver.
		'beautifulsoup4', # Beautiful Soup sits atop an HTML or XML parser, providing Pythonic idioms for iterating, searching, and modifying the parse tree.
]

# Ruby
ruby_install             = true
ruby_version             = 'latest' # Choose what ruby version should be installed (will also be the default version)
ruby_gems                = [# List any Ruby Gems that you want to install
		'bundler',
		'rails',
		'jekyll',
		'sass',
		'compass',
		'puma',
		'mailcatcher',
		'scout_realtime',
]

# Node.js
nodejs_install           = true
nodejs_version           = 'latest' # By default 'latest' will equal the latest stable version
nodejs_packages          = [# List any global NodeJS packages that you want to install
		'grunt-cli',
		'gulp',
		'bower',
		'yo',
		# 'sails',
		'forever',
		'phantomjs-prebuilt',
]


####
# Utility
##########

# Beanstalkd
beanstalkd_install       = true

# Elasticsearch
elasticsearch_install    = true
elasticsearch_version    = '2.3'


Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

	config.vm.box = box

	config.vm.provider :virtualbox do |vb|
		# Use VBoxManage to customize the VM. For example to change memory:
		vb.customize ['modifyvm', :id, '--memory', '512']
	end

	config.vm.define 'LaCodda' do |lacodda|
	end

	if Vagrant.has_plugin?('vagrant-hostmanager')
		config.hostmanager.enabled           = true
		config.hostmanager.manage_host       = true
		config.hostmanager.ignore_private_ip = false
		config.hostmanager.include_offline   = false
	end

	# Create a hostname, don't forget to put it to the `hosts` file
	# This will point to the server's default virtual host
	# TO DO: Make this work with virtualhost along-side xip.io URL
	config.vm.hostname = hostname

	# Create a static IP
	if Vagrant.has_plugin?('vagrant-auto_network')
		config.vm.network :private_network, :ip => '0.0.0.0', :auto_network => true
	else
		config.vm.network :private_network, ip: server_ip
		config.vm.network :forwarded_port, guest: 80, host: 8000
	end

	# If using VirtualBox
	config.vm.provider :virtualbox do |vb|

		vb.name = hostname+'_'+server_ip

		# Set server cpus
		vb.customize ['modifyvm', :id, '--cpus', server_cpus]

		# Set server memory
		vb.customize ['modifyvm', :id, '--memory', server_memory]

		# Set the timesync threshold to 10 seconds, instead of the default 20 minutes.
		# If the clock gets more than 15 minutes out of sync (due to your laptop going
		# to sleep for instance, then some 3rd party services will reject requests.
		vb.customize ['guestproperty', 'set', :id, '/VirtualBox/GuestAdd/VBoxService/--timesync-set-threshold', 10000]

		# Prevent VMs running on Ubuntu to lose internet connection
		# vb.customize ['modifyvm', :id, '--natdnshostresolver1', 'on']
		# vb.customize ['modifyvm', :id, '--natdnsproxy1', 'on']
	end

	# Проблема с боксом debian/jessie64 версии >=8.2.2 в том, что там не установлены VirtualBox Guest Additions.
	# Для того, чтобы решить эту проблему достаточно установить плагин vagrant-vbguest:
	# vagrant plugin install vagrant-vbguest
	# И прописать в Vagrantfile:
	# config.vm.synced_folder '.', '/vagrant', disabled: true
	# или
	# config.vm.synced_folder '.', '/vagrant', type: 'virtualbox'

	config.vm.synced_folder '.', '/vagrant', disabled: true
	# config.vm.synced_folder 'config', '/home/vagrant/Config', type: 'virtualbox'
	config.vm.synced_folder "#{host_os_document_root}/Srv", '/srv', type: 'virtualbox'
	config.vm.synced_folder "#{host_os_document_root}/Scripts", '/home/vagrant/Scripts', type: 'virtualbox'
	config.vm.synced_folder "#{host_os_document_root}/Backups", '/home/vagrant/Backups', type: 'virtualbox'


	####
	# Base Items
	##########

	# Provision Base Packages
	config.vm.provision :shell, :path => 'bootstrap/base.sh', privileged: false, :args => [server_timezone]


	####
	# Web Servers
	##########

	# Provision Nginx
	if nginx_install
		config.vm.provision :shell, :path => 'bootstrap/nginx.sh', privileged: false
	end


	####
	# Databases
	##########

	# Provision MySQL
	if mysql_install
		config.vm.provision :shell, :path => 'bootstrap/mysql.sh', privileged: false, :args => [mysql_version, mysql_root_password, mysql_enable_remote]
	end

	# Provision PostgreSQL
	if postgresql_install
		config.vm.provision :shell, :path => 'bootstrap/postgresql.sh', privileged: false, :args => [postgresql_version, postgresql_root_password]
	end

	# Provision RethinkDB
	if rethinkdb_install
		config.vm.provision :shell, :path => 'bootstrap/rethinkdb.sh', privileged: false
	end

	# Provision MongoDB
	if mongodb_install
		config.vm.provision :shell, :path => 'bootstrap/mongodb.sh', privileged: false, :args => [mongodb_version, mongodb_enable_remote]
	end

	# Provision MariaDB
	if mariadb_install
		config.vm.provision :shell, :path => 'bootstrap/mariadb.sh', privileged: false, :args => [mariadb_version, mariadb_root_password, mariadb_enable_remote]
	end

	# Provision Neo4J
	if neo4j_install
		config.vm.provision :shell, :path => 'bootstrap/neo4j.sh', privileged: false
	end


	####
	# In-Memory Stores
	##########

	# Provision Redis (with journaling and persistence)
	if redis_install
		config.vm.provision :shell, :path => 'bootstrap/redis.sh', privileged: false, :args => [redis_persistent]
	end


	####
	# Additional Languages
	##########

	# Install PHP composer_packages.join(' ')
	if php_install
		config.vm.provision :shell, :path => 'bootstrap/php.sh', privileged: false, :args => composer_packages.unshift(php_version.join('|'), php_timezone, github_token)
	end

	# Install Python
	if python_install
		config.vm.provision :shell, :path => 'bootstrap/python.sh', privileged: false, :args => python_pips.unshift(python_version)
	end

	# Install Ruby
	if ruby_install
		config.vm.provision :shell, :path => 'bootstrap/ruby.sh', privileged: false, :args => ruby_gems.unshift(ruby_version)
	end

	# Install Nodejs
	if nodejs_install
		config.vm.provision :shell, :path => 'bootstrap/nodejs.sh', privileged: false, :args => nodejs_packages.unshift(nodejs_version)
	end


	####
	# Utility
	##########

	# Install Beanstalkd
	if beanstalkd_install
		config.vm.provision :shell, :path => 'bootstrap/beanstalkd.sh', privileged: false
	end

	if elasticsearch_install
		config.vm.provision :shell, :path => 'bootstrap/elasticsearch.sh', privileged: false, :args => [elasticsearch_version]
	end

end