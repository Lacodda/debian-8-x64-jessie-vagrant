#!/usr/bin/env bash

# Contains all arguments that are passed
POSTGRESQL_ARG=($@)

# Number of arguments that are given
NUMBER_OF_ARG=${#POSTGRESQL_ARG[@]}

# Prepare the variables for installing specific PostgreSQL version and Pips
if [[ $NUMBER_OF_ARG -eq 2 ]]; then
    POSTGRESQL_VERSION=${POSTGRESQL_ARG[0]} #POSTGRESQL_VERSION
    POSTGRESQL_ROOT_PASSWORD=${POSTGRESQL_ARG[1]} #POSTGRESQL_ROOT_PASSWORD
else
    # Default PostgreSQL version when nothing is given
    POSTGRESQL_VERSION=9.5 #POSTGRESQL_VERSION
    POSTGRESQL_ROOT_PASSWORD=1111 #POSTGRESQL_ROOT_PASSWORD
fi

echo ">>> Installing PostgreSQL Server $POSTGRESQL_VERSION"

# Add PostgreSQL GPG public key
# to get latest stable
wget --quiet -O - http://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc | sudo apt-key add -

# Add PostgreSQL Apt repository
# to get latest stable
echo "deb http://apt.postgresql.org/pub/repos/apt/ jessie-pgdg main" | sudo tee /etc/apt/sources.list.d/pgdg.list

# Update Apt repos
sudo apt-get -qq update

# Install PostgreSQL
# -qq implies -y --force-yes
sudo apt-get install -qq libpq-dev python-dev
sudo apt-get install -qq postgresql postgresql-client postgresql-contrib


# Configure PostgreSQL

# Listen for localhost connections
sudo sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/g" /etc/postgresql/$POSTGRESQL_VERSION/main/postgresql.conf

# Identify users via "md5", rather than "ident", allowing us
# to make PG users separate from system users. "md5" lets us
# simply use a password
echo "host    all             all             0.0.0.0/0               md5" | sudo tee -a /etc/postgresql/$POSTGRESQL_VERSION/main/pg_hba.conf
sudo service postgresql start

# Create new superuser "vagrant"
sudo -u postgres createuser -s vagrant

# Create new user "root" w/ defined password
# Not a superuser, just tied to new db "vagrant"
sudo -u postgres psql -c "CREATE ROLE root LOGIN UNENCRYPTED PASSWORD '$POSTGRESQL_ROOT_PASSWORD' NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;"

# Make sure changes are reflected by restarting
sudo service postgresql restart