#!/usr/bin/env bash

echo ">>> Installing Neo4J"

# Install prerequisite: Java
# -qq implies -y --force-yes
sudo apt-get install -qq openjdk-7-jre-headless

# Add the Neo4J key into the apt package manager:
wget -O - https://debian.neo4j.org/neotechnology.gpg.key | sudo apt-key add -

# Add Neo4J to the Apt sources list:
echo 'deb http://debian.neo4j.org/repo stable/' > /tmp/neo4j.list
sudo mv /tmp/neo4j.list /etc/apt/sources.list.d

# Update the package manager:
sudo apt-get -qq update

# Install Neo4J:
sudo apt-get install -qq neo4j

# Start the server
sudo service neo4j-service restart