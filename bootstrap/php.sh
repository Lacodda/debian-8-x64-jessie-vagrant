#!/usr/bin/env bash

# PHP Brew Requirements
sudo apt-get build-dep -qq php5
sudo apt-get install -qq php5 php5-dev php-pear autoconf automake curl libcurl3-openssl-dev build-essential libxslt1-dev re2c libxml2 libxml2-dev php5-cli bison libbz2-dev libreadline-dev
sudo apt-get install -qq libfreetype6 libfreetype6-dev libpng12-0 libpng12-dev libjpeg-dev libjpeg8-dev libjpeg8  libgd-dev libgd3 libxpm4 libltdl7 libltdl-dev
sudo apt-get install -qq libssl-dev openssl
sudo apt-get install -qq gettext libgettextpo-dev libgettextpo0
sudo apt-get install -qq libicu-dev
sudo apt-get install -qq libmhash-dev libmhash2
sudo apt-get install -qq libmcrypt-dev libmcrypt4

# Check if PHPBrew is installed
phpbrew --version > /dev/null 2>&1
PHPBREW_IS_INSTALLED=$?

# Contains all arguments that are passed
PHP_ARG=($@)

# Number of arguments that are given
NUMBER_OF_ARG=${#PHP_ARG[@]}

# Prepare the variables for installing specific PHP version
if [[ $NUMBER_OF_ARG -gt 1 ]]; then
    # Both PHP version and Pips are given
    PHP_VERSION=${PHP_ARG[0]} #PHP_VERSION
    PHP_TIMEZONE=${PHP_ARG[1]} #PHP_TIMEZONE
    GITHUB_TOKEN=${PHP_ARG[2]} #GITHUB_TOKEN
    COMPOSER_PACKAGES=${PHP_ARG[@]:3} #COMPOSER_PACKAGES
else
    # Default PHP version when nothing is given
    PHP_VERSION=latest
    PHP_TIMEZONE=UTC
    GITHUB_TOKEN=''
fi

echo ">>> Update & Upgrade"
sudo apt-get -qq update && sudo apt-get -qq upgrade

if [[ $PHPBREW_IS_INSTALLED -eq 0 ]]; then
    echo ">>> Updating PHPBrew"
    phpbrew update
else
    curl -L -O https://github.com/phpbrew/phpbrew/raw/master/phpbrew
    chmod +x phpbrew
    sudo mv phpbrew /usr/local/bin/phpbrew
    phpbrew init

	echo 'export PHPBREW_SET_PROMPT=1' >> ~/.bashrc
	echo 'source ~/.phpbrew/bashrc' >> ~/.bashrc
	source ~/.phpbrew/bashrc

    phpbrew update
fi

PHP_VERSION_ARRAY=$(echo $PHP_VERSION | tr "|" "\n")

for PHP_VERSION in $PHP_VERSION_ARRAY; do

    echo ">>> Installing PHP version:" $PHP_VERSION
    # Install selected PHP version
    # Virtual variants:
    #   dbs:      sqlite, mysql, pgsql, pdo
    #   mb:       mbstring, mbregex
    #   neutral:
    #   default:  filter, dom, bcmath, ctype, mhash, fileinfo, pdo, posix, ipc,
    #             pcntl, bz2, zip, cli, json, mbstring, mbregex, calendar, sockets, readline, xml_all

    phpbrew install $PHP_VERSION +default +dbs +mb +fpm +soap +bcmath +mcrypt +xmlrpc +zlib +ipv6 +kerberos +openssl +pcre +phar +exif +ftp +gettext +hash +iconv +session +tokenizer +dba +debug +embed +imap +intl +cgi # +icu +apxs2 +gcov +tidy +gmp
	# Если php 5.4 то добавляем: -- --with-fpm-systemd=no
    PHP_VERSION=$(phpbrew list | grep -Eo '([0-9]{1,3}\.){2}[0-9]{1,3}' | sort | tail -1)

    phpbrew switch $PHP_VERSION

    echo ">>> Setup PHP settings"

    # PHP Error Reporting Config
    sed -i "s/error_reporting.*/error_reporting = E_ALL/" ~/.phpbrew/php/php-${PHP_VERSION}/etc/php.ini
    sed -i "s/display_errors.*/display_errors = On/" ~/.phpbrew/php/php-${PHP_VERSION}/etc/php.ini
    # PHP Date Timezone
    sed -i "s/;date.timezone.*/date.timezone = ${PHP_TIMEZONE/\//\\/}/" ~/.phpbrew/php/php-${PHP_VERSION}/etc/php.ini
    # MySQL default socket
    sed -i "s/mysql.default_socket.*/mysql.default_socket = \/var\/run\/mysqld\/mysqld.sock/" ~/.phpbrew/php/php-${PHP_VERSION}/etc/php.ini
    sed -i "s/mysqli.default_socket.*/mysqli.default_socket = \/var\/run\/mysqld\/mysqld.sock/" ~/.phpbrew/php/php-${PHP_VERSION}/etc/php.ini
    sed -i "s/pdo_mysql.default_socket.*/pdo_mysql.default_socket = \/var\/run\/mysqld\/mysqld.sock/" ~/.phpbrew/php/php-${PHP_VERSION}/etc/php.ini
    echo "include = /home/vagrant/.phpbrew/php/php-$PHP_VERSION/etc/php-fpm.d/*.conf" >> ~/.phpbrew/php/php-${PHP_VERSION}/etc/php-fpm.conf

    echo ">>> Installing PHP Extensions"

    phpbrew ext install gd -- --with-gd --with-png-dir --with-jpeg-dir --with-freetype-dir
    phpbrew ext install opcache
    phpbrew ext install memcache
    phpbrew ext install xdebug latest
    phpbrew ext install ldap -- --with-libdir=lib/x86_64-linux-gnu

    # xDebug Config
    cat > $(find ~/.phpbrew/php/php-${PHP_VERSION} -name xdebug.ini) << EOF
zend_extension=xdebug.so
xdebug.remote_enable = 1
xdebug.remote_connect_back = 1
xdebug.remote_port = 9000
xdebug.scream=0
xdebug.cli_color=1
xdebug.show_local_vars=1

; var_dump display
xdebug.var_display_max_depth = 5
xdebug.var_display_max_children = 256
xdebug.var_display_max_data = 1024
EOF
done

echo ">>> Installing PHPUnit"

phpbrew app get phpunit
PHPUNIT_ALIAS='alias phpunit="~/.phpbrew/bin/phpunit"'
echo $PHPUNIT_ALIAS >> ~/.bashrc
eval $PHPUNIT_ALIAS
phpunit --self-update

echo ">>> Installing Composer"

phpbrew app get composer
COMPOSER_ALIAS='alias composer="~/.phpbrew/bin/composer"'
echo $COMPOSER_ALIAS >> ~/.bashrc
eval $COMPOSER_ALIAS
composer self-update
composer global update

if [[ $GITHUB_TOKEN != '' ]]; then
    composer config -g github-oauth.github.com $GITHUB_TOKEN
fi

# Install Global Composer Packages if any are given
if [[ ! -z $COMPOSER_PACKAGES ]]; then
    echo ">>> Installing Global Composer Packages"
    composer global require ${COMPOSER_PACKAGES[@]}
    composer global update
    echo 'export PATH="$PATH:~/.composer/vendor/bin"' >> ~/.bashrc
    export PATH="$PATH:~/.composer/vendor/bin"
fi