#!/usr/bin/env bash

echo ">>> Installing timezone $1"
sudo ln -sf /usr/share/zoneinfo/$1 /etc/localtime

echo ">>> Installing Update keys"
sudo apt-get -qq update
sudo apt-get install -qq debian-keyring debian-archive-keyring

echo ">>> Update & Upgrade"
sudo apt-get -qq update && sudo apt-get -qq upgrade

echo ">>> Installing Basic packages"
# Base
sudo apt-get install -qq curl libcurl3 libcurl3-dev libcurl4-openssl-dev supervisor ssmtp git mc nano
sudo apt-get install -qq build-essential software-properties-common python-software-properties
sudo apt-get install -qq memcached sqlite
# Python Requirements
sudo apt-get install -qq libpq-dev python-dev
sudo apt-get install -qq make libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev libxml2-dev libxslt-dev
sudo apt-get install -qq libncurses5 libncurses5-dev libncursesw5 libncursesw5-dev
sudo apt-get install -qq qt5-default libqt5webkit5-dev build-essential xvfb