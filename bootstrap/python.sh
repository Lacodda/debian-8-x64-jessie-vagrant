#!/usr/bin/env bash

# Check if PyEnv is installed
pyenv -v > /dev/null 2>&1
PYENV_IS_INSTALLED=$?

# Contains all arguments that are passed
PYTHON_ARG=($@)

# Number of arguments that are given
NUMBER_OF_ARG=${#PYTHON_ARG[@]}

# Prepare the variables for installing specific Python version and Pips
if [[ $NUMBER_OF_ARG -gt 1 ]]; then
    # Both Python version and Pips are given
    PYTHON_VERSION=${PYTHON_ARG[0]} #PYTHON_VERSION
    PYTHON_PIPS=${PYTHON_ARG[@]:1}
elif [[ $NUMBER_OF_ARG -eq 1 ]]; then
    # Only Python version is given
    PYTHON_VERSION=$PYTHON_ARG
else
    # Default Python version when nothing is given
    PYTHON_VERSION=latest
fi

echo ">>> Update & Upgrade"
sudo apt-get -qq update && sudo apt-get -qq upgrade

if [[ $PYENV_IS_INSTALLED -eq 0 ]]; then
    echo ">>> Updating PyEnv"
    pyenv update
else
	echo ">>> Installing PyEnv"
	curl -L https://raw.githubusercontent.com/yyuu/pyenv-installer/master/bin/pyenv-installer | bash

    PYTHON_BUILD_MIRROR_URL='export PYTHON_BUILD_MIRROR_URL="http://yyuu.github.io/pythons"'
    PYENV_VIRTUALENV_DISABLE_PROMPT='export PYENV_VIRTUALENV_DISABLE_PROMPT=1'
    PYENV_PATH='export PATH="$HOME/.pyenv/bin:$PATH"'
    PYENV_INIT='pyenv init -'
    PYENV_VIRTUALENV_INIT='pyenv virtualenv-init -'

	echo $PYTHON_BUILD_MIRROR_URL >> ~/.bashrc
    echo $PYENV_VIRTUALENV_DISABLE_PROMPT >> ~/.bashrc
	echo $PYENV_PATH >> ~/.bashrc
	echo 'eval "$('$PYENV_INIT')"' >> ~/.bashrc
	echo 'eval "$('$PYENV_VIRTUALENV_INIT')"' >> ~/.bashrc
	source ~/.bashrc

	eval $PYTHON_BUILD_MIRROR_URL
	eval $PYENV_VIRTUALENV_DISABLE_PROMPT
	eval $PYENV_PATH
	eval "$($PYENV_INIT)"
	eval "$($PYENV_VIRTUALENV_INIT)"

    pyenv update
fi

if [[ $PYTHON_VERSION =~ "latest" ]]; then
	PYTHON_VERSION=$(pyenv install --list | grep -v - | grep -v b | tail -1)
fi

echo ">>> Installing Python version:" $PYTHON_VERSION
# Install selected Python version
pyenv install $PYTHON_VERSION
pyenv rehash
pyenv virtualenv $PYTHON_VERSION ${PYTHON_VERSION}_virtualenv
pyenv global ${PYTHON_VERSION}_virtualenv

# Install (optional) Python Pips
if [[ ! -z $PYTHON_PIPS ]]; then
    echo ">>> Start installing Python Pips"
    pip install --upgrade pip setuptools
    pip install -U ${PYTHON_PIPS[@]}
fi