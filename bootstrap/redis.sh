#!/usr/bin/env bash

# Contains all arguments that are passed
REDIS_ARG=($@)

# Number of arguments that are given
NUMBER_OF_ARG=${#REDIS_ARG[@]}

# Prepare the variables for installing specific Redis args
if [[ $NUMBER_OF_ARG -eq 1 ]]; then
    REDIS_PERSISTENT=${REDIS_ARG[0]} #REDIS_PERSISTENT
else
    # Default Redis args
    REDIS_PERSISTENT=false #REDIS_PERSISTENT
fi

echo ">>> Installing Redis"
sudo apt-get install -qq build-essential
mkdir /tmp/redis
cd /tmp/redis
wget http://download.redis.io/releases/redis-stable.tar.gz
tar xzf redis-stable.tar.gz
cd redis-stable
make
sudo apt-get install -qq tcl8.5
sudo make install clean

echo ">>> Setup Redis settings"
sudo mkdir /etc/redis
sudo cp redis.conf /etc/redis/redis.conf
sudo sed -i "s/daemonize no/daemonize yes/" /etc/redis/redis.conf

# Redis Configuration
sudo mkdir -p /etc/redis/conf.d

# transaction journaling - config is written, only enabled if persistence is requested
cat << EOF | sudo tee /etc/redis/conf.d/journaling.conf
appendonly yes
appendfsync everysec
EOF

# Persistence

if [ $REDIS_PERSISTENT == "true" ]; then
	echo ">>> Enabling Redis Persistence"
	# add the config to the redis config includes
	if ! cat /etc/redis/redis.conf | grep -q "journaling.conf"; then
		sudo echo "include /etc/redis/conf.d/journaling.conf" >> /etc/redis/redis.conf
	fi
	# schedule background append rewriting
	if ! crontab -l | grep -q "redis-cli bgrewriteaof"; then
		line="*/5 * * * * /usr/bin/redis-cli bgrewriteaof > /dev/null 2>&1"
		(sudo crontab -l; echo "$line" ) | sudo crontab -
	fi
fi # persistent

#sudo service redis-server restart