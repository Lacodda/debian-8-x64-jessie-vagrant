#!/usr/bin/env bash

# Contains all arguments that are passed
MYSQL_ARG=($@)

# Number of arguments that are given
NUMBER_OF_ARG=${#MYSQL_ARG[@]}

# Prepare the variables for installing specific MySQL version and Pips
if [[ $NUMBER_OF_ARG -eq 3 ]]; then
    MYSQL_VERSION=${MYSQL_ARG[0]} #MYSQL_VERSION
    MYSQL_ROOT_PASSWORD=${MYSQL_ARG[1]} #MYSQL_ROOT_PASSWORD
    MYSQL_ENABLE_REMOTE=${MYSQL_ARG[2]} #MYSQL_ENABLE_REMOTE
else
    # Default MySQL version when nothing is given
    MYSQL_VERSION=5.6 #MYSQL_VERSION
    MYSQL_ROOT_PASSWORD=1111 #MYSQL_ROOT_PASSWORD
    MYSQL_ENABLE_REMOTE=false #MYSQL_ENABLE_REMOTE
fi

if [ $MYSQL_VERSION == "5.6" ]; then
    # Add repo for MySQL 5.6
	sudo apt-key adv --keyserver pgp.mit.edu --recv-keys 5072E1F5
    echo "deb http://repo.mysql.com/apt/debian jessie mysql-$MYSQL_VERSION" | sudo tee /etc/apt/sources.list.d/mysql.list
    #gpg --keyserver subkeys.pgp.net --recv 8C718D3B5072E1F5
    #gpg --export --armor 8C718D3B5072E1F5 | sudo apt-key add -

	# Update Again
	sudo apt-get -qq update
fi

echo ">>> Installing MySQL Server $MYSQL_VERSION"

# Install MySQL Server
# -qq implies -y --force-yes
sudo DEBIAN_FRONTEND=noninteractive apt-get -y -q install mysql-server mysql-client libmysqlclient-dev libmysqld-dev
mysqladmin -u root password $MYSQL_ROOT_PASSWORD

# Make MySQL connectable from outside world without SSH tunnel
if [ $MYSQL_ENABLE_REMOTE == "true" ]; then
    # enable remote access
    # setting the mysql bind-address to allow connections from everywhere
    sudo sed -i "s/bind-address.*/bind-address = 0.0.0.0/" /etc/mysql/my.cnf
    sudo sed -i "s/sql_mode=.*/sql_mode=NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION/" /etc/mysql/my.cnf
    sudo sed -i "s/sql_mode=.*/sql_mode=NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION/" /usr/my.cnf

    # adding grant privileges to mysql root user from everywhere
    # thx to http://stackoverflow.com/questions/7528967/how-to-grant-mysql-privileges-in-a-bash-script for this
    MYSQL=`which mysql`

    Q1="GRANT ALL ON *.* TO 'root'@'%' IDENTIFIED BY '$MYSQL_ROOT_PASSWORD' WITH GRANT OPTION;"
    Q2="FLUSH PRIVILEGES;"
    SQL="${Q1}${Q2}"

    $MYSQL -uroot -p$MYSQL_ROOT_PASSWORD -e "$SQL"

    sudo service mysql restart
fi
