#!/usr/bin/env bash

# Contains all arguments that are passed
MONGODB_ARG=($@)

# Number of arguments that are given
NUMBER_OF_ARG=${#MONGODB_ARG[@]}

# Prepare the variables for installing specific MongoDB version and Pips
if [[ $NUMBER_OF_ARG -eq 2 ]]; then
    MONGODB_VERSION=${MONGODB_ARG[0]} #MONGODB_VERSION
    MONGODB_ENABLE_REMOTE=${MONGODB_ARG[1]} #MONGODB_ENABLE_REMOTE
else
    # Default MongoDB version when nothing is given
    MONGODB_VERSION=3.2 #MONGODB_VERSION
    MONGODB_ENABLE_REMOTE=false #MONGODB_ENABLE_REMOTE
fi

echo ">>> Installing MongoDB"

if [ $MONGODB_VERSION == "3.2" ]; then
    # Get key and add to sources
    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
	echo "deb http://repo.mongodb.org/apt/debian wheezy/mongodb-org/3.2 main" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list

else
# Get key and add to sources
    sudo apt-key adv --keyserver keyserver.ubuntu.com --recv 7F0CEB10
    echo 'deb http://downloads-distro.mongodb.org/repo/debian-sysvinit dist 10gen' | sudo tee /etc/apt/sources.list.d/mongodb.list
fi

# Update
sudo apt-get -qq update

# Install MongoDB
# -qq implies -y --force-yes
sudo apt-get install -qq mongodb-org

# Make MongoDB connectable from outside world without SSH tunnel
if [ $MONGODB_ENABLE_REMOTE == "true" ]; then
    # enable remote access
    # setting the mongodb bind_ip to allow connections from everywhere
    sudo sed -i "s/bind_ip = .*/bind_ip = 0.0.0.0/" /etc/mongod.conf
fi