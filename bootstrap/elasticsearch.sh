#!/usr/bin/env bash

# Contains all arguments that are passed
ELASTICSEARCH_ARG=($@)

# Number of arguments that are given
NUMBER_OF_ARG=${#ELASTICSEARCH_ARG[@]}

# Prepare the variables for installing specific Elasticsearch args
if [[ $NUMBER_OF_ARG -eq 1 ]]; then
    ELASTICSEARCH_VERSION=${ELASTICSEARCH_ARG[0]} #ELASTICSEARCH_VERSION
else
    # Default Elasticsearch args
    ELASTICSEARCH_VERSION=2.3 # Check https://www.elastic.co/downloads/elasticsearch for latest version
fi

echo ">>> Installing Elasticsearch"

# Install prerequisite: Java
# -qq implies -y --force-yes
sudo apt-get install -qq openjdk-7-jre openjdk-7-jre-headless

wget -qO - https://packages.elasticsearch.org/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb http://packages.elastic.co/elasticsearch/2.x/debian stable main" | sudo tee /etc/apt/sources.list.d/elasticsearch.list

sudo apt-get -qq update
sudo apt-get install -qq elasticsearch

# Configure Elasticsearch for development purposes (1 shard/no replicas, don't allow it to swap at all if it can run without swapping)
sudo sed -i "s/# index.number_of_shards: 1/index.number_of_shards: 1/" /etc/elasticsearch/elasticsearch.yml
sudo sed -i "s/# index.number_of_replicas: 0/index.number_of_replicas: 0/" /etc/elasticsearch/elasticsearch.yml
sudo sed -i "s/# bootstrap.mlockall: true/bootstrap.mlockall: true/" /etc/elasticsearch/elasticsearch.yml
sudo service elasticsearch restart

# Configure to start up Elasticsearch automatically
sudo update-rc.d elasticsearch defaults 95 10