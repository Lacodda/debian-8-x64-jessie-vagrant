#!/usr/bin/env bash

echo ">>> Installing RethinkDB"

# Add PPA to server
echo "deb http://download.rethinkdb.com/apt `lsb_release -cs` main" | sudo tee /etc/apt/sources.list.d/rethinkdb.list
wget -qO- https://download.rethinkdb.com/apt/pubkey.gpg | sudo apt-key add -

# Update
sudo apt-get -qq update

# Install
# -qq implies -y --force-yes
sudo apt-get install -qq rethinkdb
