#!/usr/bin/env bash

echo ">>> Installing Nginx"
wget http://nginx.org/keys/nginx_signing.key
sudo apt-key add nginx_signing.key
echo 'deb http://nginx.org/packages/mainline/debian/ jessie nginx' | sudo tee -a /etc/apt/sources.list.d/nginx.list
echo 'deb-src http://nginx.org/packages/mainline/debian/ jessie nginx' | sudo tee -a /etc/apt/sources.list.d/nginx.list
sudo apt-get -qq update
sudo apt-get install -qq nginx

# Turn off sendfile to be more compatible with Windows, which can't use NFS
sudo sed -i 's/sendfile on;/sendfile off;/' /etc/nginx/nginx.conf

# Set run-as user for PHP5-FPM processes to user/group "vagrant"
# to avoid permission errors from apps writing to files
sudo sed -i "s/user www-data;/user vagrant;/" /etc/nginx/nginx.conf
sudo sed -i "s/# server_names_hash_bucket_size.*/server_names_hash_bucket_size 64;/" /etc/nginx/nginx.conf

sudo mkdir -p /var/cache/nginx/temp/fastcgi
sudo mkdir -p /var/cache/nginx/temp/proxy
sudo chown -R www-data:www-data /var/cache/nginx
sudo chown -R vagrant:vagrant /var/cache/nginx

# Add vagrant user to www-data group
usermod -a -G www-data vagrant

# Autorestart
# sudo update-rc.d nginx defaults

sudo service nginx restart