#!/usr/bin/env bash

# Contains all arguments that are passed
MARIADB_ARG=($@)

# Number of arguments that are given
NUMBER_OF_ARG=${#MARIADB_ARG[@]}

# Prepare the variables for installing specific MariaDB version and Pips
if [[ $NUMBER_OF_ARG -eq 3 ]]; then
    MARIADB_VERSION=${MARIADB_ARG[0]} #MARIADB_VERSION
    MARIADB_ROOT_PASSWORD=${MARIADB_ARG[1]} #MARIADB_ROOT_PASSWORD
    MARIADB_ENABLE_REMOTE=${MARIADB_ARG[2]} #MARIADB_ENABLE_REMOTE
else
    # Default MariaDB version when nothing is given
    MARIADB_VERSION=10.1 #MARIADB_VERSION
    MARIADB_ROOT_PASSWORD=1111 #MARIADB_ROOT_PASSWORD
    MARIADB_ENABLE_REMOTE=false #MARIADB_ENABLE_REMOTE
fi

# Add repo for MariaDB
sudo apt-get install -qq software-properties-common
sudo apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 0xcbcb082a1bb943db
echo "deb [arch=amd64,i386] http://mirror.timeweb.ru/mariadb/repo/$MARIADB_VERSION/debian jessie main" | sudo tee /etc/apt/sources.list.d/mariadb.list

# Update
sudo apt-get -qq update

echo ">>> Installing MariaDB Server $MARIADB_VERSION"

# Install MariaDB without password prompt
# Set username to 'root' and password to 'mariadb_root_password' (see Vagrantfile)
sudo debconf-set-selections <<< "maria-db-$MARIADB_VERSION mysql-server/root_password password $MARIADB_ROOT_PASSWORD"
sudo debconf-set-selections <<< "maria-db-$MARIADB_VERSION mysql-server/root_password_again password $MARIADB_ROOT_PASSWORD"

# Install MariaDB Server
# -qq implies -y --force-yes
sudo apt-get install -qq mariadb-server

# Make MariaDB connectable from outside world without SSH tunnel
if [ $MARIADB_ENABLE_REMOTE == "true" ]; then
    # enable remote access
    # setting the mysql bind-address to allow connections from everywhere
    sudo sed -i "s/bind-address.*/bind-address = 0.0.0.0/" /etc/mysql/my.cnf

    # adding grant privileges to mysql root user from everywhere
    # thx to http://stackoverflow.com/questions/7528967/how-to-grant-mysql-privileges-in-a-bash-script for this
    MYSQL=`which mysql`

    Q1="GRANT ALL ON *.* TO 'root'@'%' IDENTIFIED BY '$MARIADB_ROOT_PASSWORD' WITH GRANT OPTION;"
    Q2="FLUSH PRIVILEGES;"
    SQL="${Q1}${Q2}"

    $MYSQL -uroot -p$MARIADB_ROOT_PASSWORD -e "$SQL"

    sudo service mysql restart
fi