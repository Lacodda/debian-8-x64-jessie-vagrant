#!/usr/bin/env bash

# Check if rbenv is installed
rbenv -v > /dev/null 2>&1
RBENV_IS_INSTALLED=$?

# Contains all arguments that are passed
RUBY_ARG=($@)

# Number of arguments that are given
NUMBER_OF_ARG=${#RUBY_ARG[@]}

# Prepare the variables for installing specific Ruby version and Gems
if [[ $NUMBER_OF_ARG -gt 1 ]]; then
    # Both Ruby version and Gems are given
    RUBY_VERSION=${RUBY_ARG[0]} #RUBY_VERSION
    RUBY_GEMS=${RUBY_ARG[@]:1}
elif [[ $NUMBER_OF_ARG -eq 1 ]]; then
    # Only Ruby version is given
    RUBY_VERSION=$RUBY_ARG
else
    # Default Ruby version when nothing is given
    RUBY_VERSION=latest
fi

echo ">>> Update & Upgrade"
sudo apt-get -qq update && sudo apt-get -qq upgrade

if [[ $RBENV_IS_INSTALLED -eq 0 ]]; then
    echo ">>> Updating rbenv"
    cd ~/.rbenv
    git pull
else
    echo ">>> Installing rbenv"

    curl -fsSL https://github.com/rbenv/rbenv-installer/raw/master/bin/rbenv-installer | bash

    RBENV_PATH='export PATH="$HOME/.rbenv/bin:$PATH"'
    RBENV_INIT='rbenv init -'

	echo $RBENV_PATH >> ~/.bashrc
	echo 'eval "$('$RBENV_INIT')"' >> ~/.bashrc
	source ~/.bashrc

	eval $RBENV_PATH
	eval "$($RBENV_INIT)"
fi

if [[ $RUBY_VERSION =~ "latest" ]]; then
	RUBY_VERSION=$(rbenv install --l | grep -v - | grep -v b | tail -1)
fi

echo ">>> Installing Ruby version:" $RUBY_VERSION
# Install selected Ruby version
rbenv install $RUBY_VERSION
rbenv global $RUBY_VERSION

# Install (optional) Ruby Gems
if [[ ! -z $RUBY_GEMS ]]; then
    echo ">>> Start installing Ruby Gems"
    gem update
    gem install ${RUBY_GEMS[@]}
fi